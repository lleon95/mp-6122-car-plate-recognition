
import cv2
from PIL import Image, ImageOps
from torch.utils.data import Dataset
import glob
import os
import numpy as np

from preprocess import get_boxes, boxes_to_output


def kitti_label_to_custom(filename, width_scaling, height_scaling, top_border):
    label = []
    with open(filename) as file:
        for line in file.readlines():
            split_line = line.split()
            if split_line[0] in ['Car', 'Van', 'Truck']:
                x_0 = int(float(split_line[4])) * width_scaling
                y_0 = int(float(split_line[5])) * height_scaling + top_border
                x_1 = int(float(split_line[6])) * width_scaling
                y_1 = int(float(split_line[7])) * height_scaling + top_border
                width = x_1 - x_0
                height = y_1 - y_0
                label.append((x_0, y_0, width, height))

    return label


class KittiCarDataset(Dataset):
    def __init__(self,
                 image_path,
                 label_path,
                 input_width,
                 input_height,
                 horz_boxes,
                 vert_boxes,
                 transform=None):
        self.image_filenames = []
        self.label_filenames = []
        self.input_width = input_width
        self.input_height = input_height
        self.horz_boxes = horz_boxes
        self.vert_boxes = vert_boxes
        for filename in glob.glob(image_path + "/*png"):
            self.image_filenames.append(filename)
            self.label_filenames.append(
                label_path +
                os.path.splitext(
                    os.path.basename(filename))[0] +
                ".txt")

        self.reference_boxes = get_boxes(
            input_width,
            input_height,
            horz_boxes,
            vert_boxes)
        self.horz_boxes = horz_boxes
        self.vert_boxes = vert_boxes
        self.transform = transform

    def __len__(self):
        return len(self.image_filenames)

    def __getitem__(self, idx):
        # Load image
        image = Image.open(self.image_filenames[idx])
        orig_shape = image.size

        # Scale to WxH without loosing aspect ration
        maxsize = (self.input_width, self.input_height)
        image.thumbnail(maxsize, Image.ANTIALIAS)
        new_shape = image.size

        # Pad with zeros up to expected image width and height
        width_scaling = new_shape[0] / orig_shape[0]
        height_scaling = new_shape[1] / orig_shape[1]

        delta_h = self.input_height - new_shape[1]
        delta_w = self.input_width - new_shape[0]

        padding = (delta_w // 2, delta_h // 2, delta_w -
                   (delta_w // 2), delta_h - (delta_h // 2))
        padded_im = np.array(ImageOps.expand(image, padding))

        label_file = self.label_filenames[idx]
        raw_label = kitti_label_to_custom(
            label_file,
            width_scaling,
            height_scaling,
            delta_h // 2)

        parsed_output = boxes_to_output(
            self.reference_boxes,
            raw_label,
            self.input_width,
            self.input_height,
            self.horz_boxes,
            self.vert_boxes)

        sample = {
            "image": padded_im,
            "box": parsed_output,
            "label_file": label_file}

        if self.transform:
            sample = self.transform(sample)

        return sample
