import os

import torch
import torch.nn as nn
import torch.optim as optim
import wandb

from model import Net
from evaluate import get_metrics, update_map
from mean_average_precision.mean_average_precision.detection_map import DetectionMAP


N_CLASSES = 1
MAP_PATIENCE = 10
MAP_TOLERANCE = 0.01
MAX_EPOCHS = 1000


def train(
        train_data_loader,
        test_data_loader,
        batch_size,
        mini_batch_size,
        device,
        box_width,
        box_height,
        reference_boxes,
        saved_model):

    model = Net(3).to(device)

    if saved_model is not None:
        if os.path.isfile(saved_model):
            checkpoint = torch.load(saved_model)
            model.load_state_dict(checkpoint)

    # Log metrics with wandb
    wandb.watch(model)

    biggest_map = -1
    biggest_map_epoch = 0

    for epoch in range(MAX_EPOCHS):  # loop over the dataset multiple times
        criterion = nn.MSELoss()
        optimizer = optim.Adam(model.parameters())

        detection_map = DetectionMAP(N_CLASSES)

        running_loss = 0.0
        batched_samples = 0
        # zero the parameter gradients
        optimizer.zero_grad()
        for i_batch, sample_batch in enumerate(train_data_loader):
            batched_samples += batch_size
            input_batch = sample_batch["image"]
            labels_batch = sample_batch["box"]

            training_labels = []
            network_outputs = []


            # forward + backward
            outputs = model(input_batch)
            loss = criterion(outputs, labels_batch)
            loss.backward()

            if batched_samples >= batch_size:
                batched_samples = 0
                optimizer.step()
                optimizer.zero_grad()

            training_labels.append(labels_batch)
            network_outputs.append(outputs)

            # print statistics
            running_loss += loss.item()

            try:
                update_map(
                    detection_map,
                    training_labels,
                    network_outputs,
                    mini_batch_size,
                    box_width,
                    box_height,
                    reference_boxes)
            except BaseException:
                pass

        train_ap, train_precision, train_recall = get_metrics(detection_map)
        wandb.log({"Train mAP": train_ap})
        wandb.log({"Train Precision": train_precision})
        wandb.log({"Train Recall": train_recall})
        wandb.log({"Train Loss": running_loss})

        print(
            "Epoch: {}\tTrain mAP: {:.3f}\tTrain Precision: {:.3f}\tTrain Recall: {:.3f}\tTrain Loss: {:.3f}".format(
                epoch,
                train_ap,
                train_precision,
                train_recall,
                running_loss))
        with torch.no_grad():

            detection_map = DetectionMAP(N_CLASSES)

            running_loss = 0.0
            for i_batch, sample_batch in enumerate(test_data_loader):
                input_batch = sample_batch["image"]
                labels_batch = sample_batch["box"]

                training_labels = []
                network_outputs = []

                # forward
                outputs = model(input_batch)
                loss = criterion(outputs, labels_batch)

                training_labels.append(labels_batch)
                network_outputs.append(outputs)

                # print statistics
                running_loss += loss.item()

                update_map(
                    detection_map,
                    training_labels,
                    network_outputs,
                    batch_size,
                    box_width,
                    box_height,
                    reference_boxes)
            test_ap, test_precision, test_recall = get_metrics(detection_map)

            wandb.log({"Test Loss": running_loss})
            wandb.log({"Test mAP": test_ap})
            wandb.log({"Test Precision": test_precision})
            wandb.log({"Test Recall": test_recall})

            print(
                "Epoch: {}\tTest mAP:  {:.3f}\tTest Precision:  {:.3f}\tTest Recall:  {:.3f}\tTest Loss:  {:.3f}".format(
                    epoch,
                    test_ap,
                    test_precision,
                    test_recall,
                    running_loss))

        # Early Stopping
        if train_ap > (biggest_map + MAP_TOLERANCE):
            biggest_map = train_ap
            biggest_map_epoch = epoch

        if epoch % 10 == 0:
            # Save model to wandb
            torch.save(model.state_dict(), os.path.join(
                os.getcwd(), 'model-{}-{}.pt'.format(epoch, MAX_EPOCHS)))

        if (epoch - biggest_map_epoch) > MAP_PATIENCE:
            break

    # Save model to wandb
    torch.save(model.state_dict(), os.path.join(wandb.run.dir, 'model.pt'))
    print('Finished Training')
