

import numpy as np


# Ideally boxes match the distribution in the dataset, maybe KNN?,
# this could also be gotten from the user
def get_boxes(input_width, input_height, boxes_x, boxes_y):
    # Create empty placeholder list
    boxes = []

    # This creates the boxes based on a uniform distribution
    for i in range(boxes_x):
        row = []
        for j in range(boxes_y):
            row.append((i * (input_width / boxes_x),
                        j * (input_height / boxes_y)))
        boxes.append(row)

    return boxes


def boxes_to_output(
        reference_boxes,
        label,
        input_width,
        input_height,
        boxes_x,
        boxes_y):
    all_labels = []

    box_width = (input_width / boxes_x)
    box_height = (input_height / boxes_y)

    # If there are more than 16 boxes or more than one box matches the
    # image section this is an undefined behaviour
    # Create label placeholder
    output_label = np.zeros((boxes_x, boxes_y, 5))
    for item in label:
        x, y, width, height = item

        # Select the most appropiate box, which has the lower mse
        # between the label and the default_box
        (min_i, min_j) = (0, 0)
        min_mse = np.infty
        for i in range(boxes_x):
            for j in range(boxes_y):
                mse = abs(x - reference_boxes[i][j][0]) + \
                    abs(y - reference_boxes[i][j][1])
                if min_mse > mse:
                    (min_i, min_j) = (i, j)
                    min_mse = mse

        # confidence, dx, dy, sx, sy]
        output_label[min_i,
                     min_j] = np.array([1,
                                        (x - reference_boxes[min_i][min_j][0]) / box_width,
                                        (y - reference_boxes[min_i][min_j][1]) / box_height,
                                        width / box_width,
                                        height / box_height])

    return output_label
