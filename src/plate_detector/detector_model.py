import torch.nn as nn
import torch.nn.functional as F

# The most important parameter is the depth output layer
# Values are modeled as:
# [conf, dx, dy, sy, sx]
# width and height of the depth layer determine number of layers

# Input images are 512x512


class DetectorNet(nn.Module):
    def __init__(self, input_channels):
        super(DetectorNet, self).__init__()
        # 640x320x3
        self.conv1 = nn.Conv2d(in_channels=input_channels,
                               out_channels=128,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.conv2 = nn.Conv2d(in_channels=128,
                               out_channels=64,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.batchnorm1 = nn.BatchNorm2d(64)
        self.pool1 = nn.MaxPool2d(2, 2)

        # 320x160x128
        self.conv3 = nn.Conv2d(in_channels=64,
                               out_channels=128,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.conv4 = nn.Conv2d(in_channels=128,
                               out_channels=64,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.batchnorm2 = nn.BatchNorm2d(64)
        self.pool2 = nn.MaxPool2d(2, 2)

        # 160x80x128
        self.conv5 = nn.Conv2d(in_channels=64,
                               out_channels=256,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.conv6 = nn.Conv2d(in_channels=256,
                               out_channels=128,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.batchnorm3 = nn.BatchNorm2d(128)
        self.pool3 = nn.MaxPool2d(2, 2)

        # 80x40x64
        self.conv7 = nn.Conv2d(in_channels=128,
                               out_channels=256,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.conv8 = nn.Conv2d(in_channels=256,
                               out_channels=128,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.batchnorm4 = nn.BatchNorm2d(128)
        self.pool4 = nn.MaxPool2d(2, 2)

        # 40x20x64
        self.conv9 = nn.Conv2d(in_channels=128,
                               out_channels=128,
                               kernel_size=(3, 3),
                               stride=1,
                               padding=1,
                               padding_mode="zeros")
        self.conv10 = nn.Conv2d(in_channels=128,
                                out_channels=64,
                                kernel_size=(3, 3),
                                stride=1,
                                padding=1,
                                padding_mode="zeros")
        self.batchnorm5 = nn.BatchNorm2d(64)
        self.pool5 = nn.MaxPool2d(2, 2)

        # 20x10x64
        self.conv11 = nn.Conv2d(in_channels=64,
                                out_channels=128,
                                kernel_size=(3, 3),
                                stride=1,
                                padding=1,
                                padding_mode="zeros")
        self.conv12 = nn.Conv2d(in_channels=128,
                                out_channels=64,
                                kernel_size=(3, 3),
                                stride=1,
                                padding=1,
                                padding_mode="zeros")
        self.batchnorm6 = nn.BatchNorm2d(64)
        self.pool6 = nn.MaxPool2d(2, 2)

        # 10x5x24
        self.conv13 = nn.Conv2d(in_channels=64,
                                out_channels=128,
                                kernel_size=(3, 3),
                                stride=1,
                                padding=1,
                                padding_mode="zeros")
        self.conv14 = nn.Conv2d(in_channels=128,
                                out_channels=5,
                                kernel_size=(3, 3),
                                stride=1,
                                padding=1,
                                padding_mode="zeros")

        # Output is 4x4x5

    def forward(self, x):
        x = F.leaky_relu(self.conv1(x))
        x = F.leaky_relu(self.conv2(x))
        x = self.batchnorm1(x)
        x = self.pool1(x)

        x = F.leaky_relu(self.conv3(x))
        x = F.leaky_relu(self.conv4(x))
        x = self.batchnorm2(x)
        x = self.pool2(x)

        x = F.leaky_relu(self.conv5(x))
        x = F.leaky_relu(self.conv6(x))
        x = self.batchnorm3(x)
        x = self.pool3(x)

        x = F.leaky_relu(self.conv7(x))
        x = F.leaky_relu(self.conv8(x))
        x = self.batchnorm4(x)
        x = self.pool4(x)

        x = F.leaky_relu(self.conv9(x))
        x = F.leaky_relu(self.conv10(x))
        x = self.batchnorm5(x)
        x = self.pool5(x)

        x = F.leaky_relu(self.conv11(x))
        x = F.leaky_relu(self.conv12(x))
        x = self.batchnorm6(x)
        x = self.pool6(x)

        x = F.leaky_relu(self.conv13(x))
        x = F.leaky_relu(self.conv14(x))

        return x
