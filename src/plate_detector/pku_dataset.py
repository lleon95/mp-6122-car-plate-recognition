
from PIL import Image, ImageOps
from torch.utils.data import Dataset
import cv2 as cv
import glob
import numpy as np
import os

from preprocess import get_boxes, boxes_to_output


def pku_mask_to_label(
        label_file,
        width_scaling,
        height_scaling,
        top_border,
        left_border):
    labels = []

    image_mask = cv.imread(label_file, cv.IMREAD_GRAYSCALE)
    ret, image_mask = cv.threshold(image_mask, 192, 255, cv.THRESH_BINARY)
    contours, _ = cv.findContours(
        image_mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    for i, c in enumerate(contours):
        boundRect = cv.boundingRect(cv.approxPolyDP(c, 3, True))

        x = int(float(boundRect[0])) * width_scaling + left_border
        y = int(float(boundRect[1])) * height_scaling + top_border
        width = int(float(boundRect[2])) * width_scaling
        height = int(float(boundRect[3])) * height_scaling
        labels.append((x, y, width, height))

    return labels


class PKUDataset(Dataset):
    def __init__(self,
                 image_path,
                 label_path,
                 input_width,
                 input_height,
                 horz_boxes,
                 vert_boxes,
                 transform=None):
        self.image_filenames = []
        self.label_filenames = []
        self.input_width = input_width
        self.input_height = input_height
        self.horz_boxes = horz_boxes
        self.vert_boxes = vert_boxes
        for filename in glob.glob(image_path + "/*jpg"):
            self.image_filenames.append(filename)
            if label_path is not None:
                self.label_filenames.append(
                    label_path + "/" +
                    os.path.splitext(
                        os.path.basename(filename))[0] +
                    ".jpg")

        self.reference_boxes = get_boxes(
            input_width,
            input_height,
            horz_boxes,
            vert_boxes)
        self.horz_boxes = horz_boxes
        self.vert_boxes = vert_boxes
        self.transform = transform

    def __len__(self):
        return len(self.image_filenames)

    def __getitem__(self, idx):
        # Load image
        image = Image.open(self.image_filenames[idx])
        orig_shape = image.size

        # Scale to WxH without loosing aspect ration
        maxsize = (self.input_width, self.input_height)
        image.thumbnail(maxsize, Image.ANTIALIAS)
        new_shape = image.size

        # Pad with zeros up to expected image width and height
        width_scaling = new_shape[0] / orig_shape[0]
        height_scaling = new_shape[1] / orig_shape[1]

        delta_h = self.input_height - new_shape[1]
        delta_w = self.input_width - new_shape[0]

        padding = (delta_w // 2, delta_h // 2, delta_w -
                   (delta_w // 2), delta_h - (delta_h // 2))
        padded_im = np.array(ImageOps.expand(image, padding))

        if len(self.label_filenames) > 0:
            label_file = self.label_filenames[idx]
            raw_label = pku_mask_to_label(
                label_file,
                width_scaling,
                height_scaling,
                delta_h // 2,
                delta_w // 2)

            parsed_output = boxes_to_output(
                self.reference_boxes,
                raw_label,
                self.input_width,
                self.input_height,
                self.horz_boxes,
                self.vert_boxes)
        else:
            parsed_output = None
            label_file = None

        sample = {
            "image": padded_im,
            "box": parsed_output,
            "label_file": label_file}

        if self.transform:
            sample = self.transform(sample)

        return sample
