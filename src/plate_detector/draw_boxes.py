
import cv2
from preprocess import get_boxes


def label_to_boxes(
        label,
        box_width,
        box_height,
        reference_boxes,
        confidence=0.7):
    boxes = []

    for i in range(label.shape[0]):
        for j in range(label.shape[1]):
            if (label[i][j][0] > confidence):
                x = int(label[i][j][1] * box_width + reference_boxes[i][j][0])
                y = int(label[i][j][2] * box_height + reference_boxes[i][j][1])
                width = int(label[i][j][3] * box_width)
                height = int(label[i][j][4] * box_height)
                boxes.append((x, y, width, height))

    return boxes


def draw_box(image, boxes, reference_boxes, box_width, box_height):
    """Draws a box on an image

    Parameters
    ----------
    image : np.array
        Image over which to draw the label
    boxes : list of touples
        each touple should have the following format: x, y, width, height
    reference_boxes: list of touples
        a list of boxes location
    box_width:
        width for the reference boxes
    box_height:
        height for the reference boxes

    Returns
    -------
    np.array
        image with the label drawn over it
    """
    image = image.copy()

    for box in boxes:
        x, y, width, height = box

        pt1 = (x, y)
        pt2 = (x + width, y + height)

        cv2.rectangle(image, pt1, pt2, 255)

    return image
