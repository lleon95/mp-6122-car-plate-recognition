#!/usr/bin/env python3

# Example of app integration
# By: Luis Leon & Emmanuel Madrigal

import argparse
import glob
import time
import sys

import cv2 as cv
import torch

import LicencePlateAnalyser
import WebSiteStarter
from plate_detector.inference import run_inference


def main(image_path):
    # Start the website
    web = WebSiteStarter.WebService()

    # setting device on GPU if available, else CPU
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('Using device:', device)

    filenames = []
    
    for filename in glob.glob(image_path + "/*jpg"):
        filenames.append(filename)
    for filename in glob.glob(image_path + "/*png"):
        filenames.append(filename)

    # Start inference
    for i, img_path in enumerate(filenames):
        # Get the plate
        img = cv.imread(img_path)

        licence_plate_imgs, bbox_image = run_inference(img, device)
        
        cv.imshow("Detected Plates", bbox_image)
        cv.imwrite("detected_plate_{}.png".format(i), bbox_image)
        for image in licence_plate_imgs:
            # Find the bounding boxes where the character locations
            bbs, preproc = LicencePlateAnalyser.find_characters(image, invert=False)
            # Inference on the plate number
            plate_num = LicencePlateAnalyser.get_plate_number(bbs, preproc, device)

            print(plate_num)


            cv.imshow("License Plate", image)
            cv.imwrite("license_plate_{}.png".format(i), image)
            # cv.imshow("preproc", preproc)
            c = cv.waitKey()
            if c == 113:
                sys.exit(0)

            # Send data
            web.send_data(plate_num)
            print("Plate:", plate_num)
        time.sleep(1)

    # Finalise website
    del web


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Detect and decypher the licence plate number')
    parser.add_argument('--images_path', type=str,
                        help='Location for the car images', required=True)
    args = parser.parse_args()

    main(args.images_path)
