/*
    Webserver - Main script
    By: Luis Leon & Emmanuel Madrigal
*/

const WEB_SERVER_PORT = 3333;

/*
    Express JS set up
*/
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(WEB_SERVER_PORT);
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

const events = require('events');
var comm_event = new events.EventEmitter();

/**
 * Back-end routes
 */
require("./back-end/websocket.js")(io, comm_event);
require("./back-end/tcpsocket.js")(comm_event);

/**
 * Front-end routes
 */
require("./back-end/site")(app);

console.log("# HTTP Server: Successfully up");
console.log("# HTTP Server: Listening on: http://localhost:" + WEB_SERVER_PORT.toString());