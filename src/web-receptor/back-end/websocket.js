/*
    Webserver - Websocket script
    By: Luis Leon & Emmanuel Madrigal
*/

function websocket(io, event) {
    io.on('connection', (socket) => {
        console.log('A user is connected');
    });

    event.on("new_data", (data) => {
        var str = data.toString();
        io.emit('new_entry', str);
    });
}

module.exports = websocket