# Plate morphological treatment
# By: Luis Leon & Emmanuel Madrigal

import cv2 as cv
import numpy as np

def resize(img):
    dim = (256,130)
    return cv.resize(img, dim)
  
def preproc(img, invert=True):
    flags = cv.THRESH_OTSU
    if invert:
        flags += cv.THRESH_BINARY_INV
    _, otsu = cv.threshold(img,0,255,flags)
    kernel2 = np.ones((3,3),np.uint8)
    opening = cv.morphologyEx(otsu,cv.MORPH_OPEN, kernel2, iterations = 1)
    _, output = cv.threshold(opening,0,255,cv.THRESH_OTSU)
    return output