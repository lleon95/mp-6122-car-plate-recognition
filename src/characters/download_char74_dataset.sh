echo "Downloading the data from the CHAR74 repository"

# Create the folder
echo "Creating data folder"
mkdir data &> /dev/null || echo "> Data folder already exists"
cd data

# Get the dataset
echo "Downloading dataset"
ls EnglishFnt.tgz &> /dev/null || \
wget "http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/EnglishFnt.tgz" -O "EnglishFnt_tmp.tgz"
mv EnglishFnt_tmp.tgz EnglishFnt.tgz

# Decompress dataset
echo "Decompressing"
tar zxf EnglishFnt.tgz

# Move data of interest
mkdir CHARS74
for i in $(seq 37 1 62);
do
    rm -r "English/Fnt/Sample0${i}"
done
cp -r English/Fnt/* CHARS74

echo "Done!"