# Resize the character detection accordingly to the input of the CNN
# By: Luis Leon & Emmanuel Madrigal

import cv2 as cv
import numpy as np

def resize_digit(img):
    size = np.shape(img)
    black = np.zeros((28,28))
    h, w = size

    if len(size) == 3:
        _ , c = size
        if c == 3:
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    else:
        gray = img

    if h == 28 and w == 28:
        return gray

    char = cv.resize(gray, (20,20))
    black[4:24, 4:24] = char
    return black

def normalise(img):
    to_float = img.astype(np.float32)
    to_float /= 255.0

    return to_float

def erode(img):
    kernel2 = np.ones((5,5),np.uint8)
    return cv.erode(img, kernel2, iterations = 1)

def preprocess_digit(img):
    eroded = erode(img)
    resized = resize_digit(eroded)
    normalised = normalise(resized)
    return normalised
