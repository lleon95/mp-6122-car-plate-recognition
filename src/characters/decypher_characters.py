#!/usr/bin/env python3

# Inference by using the CHARS74 recognition
# Inspired by LeNet 
# http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf
# http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/
#
# By: Luis Leon & Emmanuel Madrigal

import argparse
import cv2 as cv
import copy
import matplotlib.pyplot as plt
import numpy as np

import inference
import find_characters as Finder
import plate_processing as PlateProcessor
import char_processing as CharProcessor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Decypher the licence plate number')
    parser.add_argument('--image', type=str,
                        help='Location for the plate image', required=True)
    args = parser.parse_args()

    print("Reading the licence plate")
    gray = cv.imread(args.image, 0) # 0 means Grayscale

    print("Performing preprocessing on the licence plate")
    resized = PlateProcessor.resize(gray)
    opening = PlateProcessor.preproc(resized)
    preprocessed = copy.deepcopy(opening)

    print("Finding bounding boxes")
    _, bbs = Finder.bounding_boxes(resized, opening)

    
    chars = []
    for bb in bbs:
        cropped = preprocessed[bb[0][1]:bb[1][1], bb[0][0]:bb[1][0]]
        prec = CharProcessor.preprocess_digit(cropped)
        plt.imshow(prec)
        plt.show()
        character = inference.predict_char(prec)
        chars.append(character)
    
    print(chars)
    
