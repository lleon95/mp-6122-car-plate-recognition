PROJECT = autosystems
TEX = pdflatex
BIB = bibtex
FLAGS = --shell-escape


.PHONY = all okular open clean-all clean skim indent

$(PROJECT).pdf: $(PROJECT).tex macros.tex bib
		$(TEX) $(FLAGS) $(PROJECT).tex
		$(TEX) $(FLAGS) $(PROJECT).tex


all: bib
		$(TEX) $(FLAGS) $(PROJECT).tex

first_run:
		$(TEX) $(FLAGS) $(PROJECT).tex

bib: first_run
		$(BIB) $(PROJECT)

okular:
		okular $(PROJECT).pdf

indent: $(PROJECT).tex
	latexindent -s -m --overwrite $<

open:
		open $(PROJECT).pdf

skim:
		open -a Skim $(PROJECT).pdf
		defaults write -app Skim SKAutoReloadFileUpdate -boolean true

clean-all:
		$(RM) -f *.dvi *.log *.bak *.aux *.bbl *.blg *.idx *.ps *.pdf *.toc *.out \
		*~ *.synctex.gz *.fls *.fdb_latexmk

clean:
		$(RM) *.log *.bak *.aux *.bbl *.blg *.idx *.toc *.out *~ *.synctex.gz *.fls \
		*.fdb_latexmk
